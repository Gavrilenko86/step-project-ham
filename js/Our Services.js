const tabsServices = () => {
    const head = document.querySelector('.our-services__menu');
    const body = document.querySelector('#tabsContent');
  
    const setActiveTab = (tab) => {
      const activeTab = head.querySelector('.active');
      if (activeTab) {
        activeTab.classList.remove('active');
      }
      tab.classList.add('active');
    };
  
    const setActiveContent = (activeTabName) => {
      const activeContent = body.querySelector('.active');
      if (activeContent) {
        activeContent.classList.remove('active');
      }
      const content = body.querySelector(`[data="${activeTabName}"]`);
      if (content) {
        content.classList.add('active');
      }
    };
  
    const handleTabClick = (event) => {
      const tab = event.target.closest('.our-services__menu_list');
      if (!tab || tab.classList.contains('active')) {
        return;
      }
      setActiveTab(tab);
      setActiveContent(tab.getAttribute('data'));
    };
  
    const updateTabContent = (activeTabName) => {
      const tabContents = body.querySelectorAll('.our-services__content');
      tabContents.forEach((content) => {
        if (content.getAttribute('data') === activeTabName) {
          content.style.display = 'block';
        } else {
          content.style.display = 'none';
        }
      });
    };
  
    head.addEventListener('click', handleTabClick);
  };
  
  tabsServices();
